package com.demoproject.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import com.demoproject.entities.Category;
import com.demoproject.entities.Post;
import com.demoproject.entities.User;
import com.demoproject.payload.PostDtoInterface;

public interface PostRepository extends JpaRepository<Post, Long> {

	List<PostDtoInterface> findByPostId(Long postId, Class<PostDtoInterface> class1);

	Page<PostDtoInterface> findByOrderByPostIdDesc(Pageable pageable, Class<PostDtoInterface> class1);

	List<PostDtoInterface> findByUser(User user, Class<PostDtoInterface> class1);

	List<PostDtoInterface> findByCategory(Category category, Class<PostDtoInterface> class1);

	List<PostDtoInterface> findByTitleContaining(String keyword, Class<PostDtoInterface> class1);

}
