package com.demoproject.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.demoproject.entities.UserRole;
import com.demoproject.payload.UserRoleDtoInterface;

public interface UserRoleRepository extends JpaRepository<UserRole, Long> {

	@Query(value = "select ur.user_id as userId ,ur.role_id as roleId from user_role ur", nativeQuery = true)
	List<UserRoleDtoInterface> findByOrderByUserRoleIdDesc(Class<UserRoleDtoInterface> class1);

	@Query(value = "select ur.user_id as userId ,ur.role_id as roleId from user_role ur where ur.user_id=:userId", nativeQuery = true)
	List<UserRoleDtoInterface> findByUserId(@Param("userId") Long userId, Class<UserRoleDtoInterface> class1);

	@Query(value = "select ur.user_id as userId ,ur.role_id as roleId from user_role ur where ur.role_id=:roleId", nativeQuery = true)
	List<UserRoleDtoInterface> findByRoleId(@Param("roleId") Long roleId, Class<UserRoleDtoInterface> class1);
}
