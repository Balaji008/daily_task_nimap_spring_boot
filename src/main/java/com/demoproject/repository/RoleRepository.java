package com.demoproject.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.demoproject.entities.Role;
import com.demoproject.payload.RoleDtoInterface;

public interface RoleRepository extends JpaRepository<Role, Long> {

	List<RoleDtoInterface> findByRoleId(Long roleId, Class<RoleDtoInterface> class1);

	List<RoleDtoInterface> findByOrderByRoleIdDesc(Class<RoleDtoInterface> class1);
}
