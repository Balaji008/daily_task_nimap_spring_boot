package com.demoproject.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.demoproject.entities.User;
import com.demoproject.payload.UserDtoInterface;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

	List<UserDtoInterface> findById(Long userId, Class<UserDtoInterface> class1);

	List<UserDtoInterface> findByOrderByIdDesc(Class<UserDtoInterface> class1);

	User findByEmail(String email);

	Boolean existsByName(String username);

	Boolean existsByEmail(String email);

	User findByEmailIgnoreCase(String email);
}
