package com.demoproject.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.demoproject.entities.Employee;

public interface EmployeeRepository extends JpaRepository<Employee, Integer> {

	// @Query(value="select * from employee where is_active=true",nativeQuery=true)
	// public List<Employee> findAllEmployee();

	public Employee findById(int id);

	@Query(value = "update employee set is_active=false where emp_id=:id", nativeQuery = true)
	public void deleteEmployeeById(@Param("id") int id);

	@Transactional
	@Modifying
	@Query("Update Employee e set e.isActive = false where e.id=:id")
	public void deleteEmployeeByID(@Param("id") int id);

	// public void updateIsActiveById(int id);

	public List<Employee> findByIsActive(boolean flag);

}
