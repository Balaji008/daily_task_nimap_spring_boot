package com.demoproject.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.demoproject.entities.Permission;
import com.demoproject.payload.PermissionDtoInterface;

public interface PermissionRepository extends JpaRepository<Permission, Long> {

	List<PermissionDtoInterface> findByOrderByIdDesc(Class<PermissionDtoInterface> class1);

	List<PermissionDtoInterface> findById(Long id, Class<PermissionDtoInterface> class1);
}
