package com.demoproject.repository;

import java.util.LinkedHashSet;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.demoproject.entities.RolePermission;
import com.demoproject.payload.IListRolePermissionDto;
import com.demoproject.payload.RolePermissionDtoInterface;

public interface RolePermissionRepository extends JpaRepository<RolePermission, Long> {

	@Query(value = "select rp.id, rp.role_id as RoleId,r.name as RoleName,rp.permission_id as PermissionId,p.action_name as PermissionName from role_permission rp join roles r on rp.role_id=r.role_id join permissions p\r\n"
			+ "on rp.permission_id=p.id order by rp.id desc", nativeQuery = true)
	List<IListRolePermissionDto> findByOrderByIdDesc(Class<IListRolePermissionDto> class1);

	@Query(value = "select r.role_id as roleId,r.permission_id as permissionId from role_permission r where r.id=:id", nativeQuery = true)
	List<RolePermissionDtoInterface> findByRolePermissionId(@Param("id") Long id,
			Class<RolePermissionDtoInterface> class1);

	@Query(value = "select r.role_id as roleId,r.permission_id as permissionId from role_permission r where r.role_id=:roleId", nativeQuery = true)
	List<RolePermissionDtoInterface> findByRoleId(@Param("roleId") Long roleId,
			Class<RolePermissionDtoInterface> class1);

	@Query(value = "select r.role_id as roleId,r.permission_id as permissionId from role_permission r where r.permission_id=:permissionId", nativeQuery = true)
	List<RolePermissionDtoInterface> findByPermissionId(@Param("permissionId") Long permissionId,
			Class<RolePermissionDtoInterface> class1);

//	This query also returns same result but shows duplicate permissions and it is a complex query than below query
//	@Query(value = "select p.action_name as PermissionName \r\n" + "from (\r\n" + "  select ur.user_id, ur.role_id\r\n"
//			+ "  from user_role ur \r\n" + "  join users u on ur.user_id = u.id \r\n"
//			+ "  join roles r on ur.role_id = r.role_id where u.id =:userId\r\n" + ") as sub \r\n"
//			+ "join roles r on sub.role_id = r.role_id \r\n" + "join users u on sub.user_id = u.id \r\n"
//			+ "join role_permission rp on sub.role_id = rp.role_id \r\n"
//			+ "join permissions p on rp.permission_id = p.id \r\n" + "order by rp.id desc", nativeQuery = true)

	@Query(value = "select distinct p.action_name from role_permission rp join permissions p on rp.permission_id=p.id join roles r on rp.role_id=r.role_id \r\n"
			+ "where r.role_id in (select r.role_id from user_role ur join users u on ur.user_id=u.id join roles r on ur.role_id=r.role_id where u.id=:userId)\r\n"
			+ "order by p.action_name desc", nativeQuery = true)
	LinkedHashSet<String> findPermissionsByUserId(@Param("userId") Long userId);

}
