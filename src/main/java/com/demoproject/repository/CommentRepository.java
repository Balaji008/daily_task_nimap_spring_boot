package com.demoproject.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.demoproject.entities.Comment;

public interface CommentRepository extends JpaRepository<Comment, Long>{

}
