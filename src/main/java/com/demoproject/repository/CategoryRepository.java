package com.demoproject.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.demoproject.entities.Category;
import com.demoproject.payload.CategoryDtoInterface;

public interface CategoryRepository extends JpaRepository<Category, Long> {

	List<CategoryDtoInterface> findByCategoryId(Long categoryId, Class<CategoryDtoInterface> class1);

	List<CategoryDtoInterface> findByOrderByCategoryIdDesc(Class<CategoryDtoInterface> class1);

}
