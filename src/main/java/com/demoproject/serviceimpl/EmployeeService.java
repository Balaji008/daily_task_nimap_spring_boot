package com.demoproject.serviceimpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.demoproject.entities.Employee;
import com.demoproject.repository.EmployeeRepository;
import com.demoproject.serviceinterfaces.EmployeeServiceInterface;

@Service
public class EmployeeService implements EmployeeServiceInterface {

	@Autowired
	private EmployeeRepository employeerepository;

	@Override
	public List<Employee> getAllEmployees() {
		
		List<Employee> list = this.employeerepository.findByIsActive(true);
		
		return list;

	}

	@Override
	public Employee getEmployeeById(int id) {
		Employee emp = null;
		try {
			emp = this.employeerepository.findById(id);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return emp;
	}

	@Override
	public Employee addEmployee(Employee emp) {

		Employee result = this.employeerepository.save(emp);
		return result;

	}

	@Override
	public void deleteEmployeeById(int id) {
		this.employeerepository.deleteEmployeeByID(id);
	}

	@Override
	public Employee updateEmployeById(Employee emp, int id) {
		emp.setId(id);
		this.employeerepository.save(emp);
		return emp;
	}
	
	public void deleteEmployeeByID(int id)
	{
		Employee emp = null;
		try {
			emp = this.employeerepository.findById(id);
			emp.setActive(false);
			this.employeerepository.save(emp);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}

}
