package com.demoproject.serviceimpl;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.UUID;

import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.demoproject.serviceinterfaces.FileServiceInterface;

@Service
public class FileServiceImpl implements FileServiceInterface {

	@Override
	public String uploadImage(String path, MultipartFile file) throws IOException {

		// File Name
		String name = file.getOriginalFilename();

		// Random Name genarate File
		String randomId = UUID.randomUUID().toString();
		String fileName1 = randomId.concat(name.substring(name.lastIndexOf(".")));

		// Full Path
		String filePath = path + File.separator + fileName1;

		// Create Folder if not created
		File f = new File(path);

		if (!f.exists()) {
			f.mkdir();
		}

		Files.copy(file.getInputStream(), Paths.get(filePath));
		return randomId;
	}

	@Override
	public InputStream getResource(String path, String fileName) throws FileNotFoundException {

		String fullPath = path + File.separator + fileName;

		InputStream inputStream = new FileInputStream(fullPath);
		return inputStream;
	}

}
