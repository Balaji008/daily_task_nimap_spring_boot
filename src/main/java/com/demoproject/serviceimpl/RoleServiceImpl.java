package com.demoproject.serviceimpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.demoproject.entities.Role;
import com.demoproject.exceptions.ResourceNotFoundException;
import com.demoproject.payload.RoleDto;
import com.demoproject.payload.RoleDtoInterface;
import com.demoproject.repository.RoleRepository;
import com.demoproject.serviceinterfaces.RoleServiceInterface;

@Service
public class RoleServiceImpl implements RoleServiceInterface {

	@Autowired
	private RoleRepository roleRepository;

	@Override
	public RoleDto addRole(RoleDto roleDto) {

		Role role = new Role();
		role.setName(roleDto.getName());
		this.roleRepository.save(role);
		return roleDto;
	}

	@Override
	public RoleDto updateRole(RoleDto roleDto ,Long roleId) {

		Role role = new Role();
		Role role2 = this.roleRepository.findById(roleId)
				.orElseThrow(() -> new ResourceNotFoundException("Role", "Role Id", roleId));
		role.setName(role2.getName());
		this.roleRepository.save(role);
		return roleDto;
	}

	@Override
	public RoleDto deleteRole(Long roleId) {
		Role role = null;
		RoleDto roleDto = new RoleDto();
		role = this.roleRepository.findById(roleId)
				.orElseThrow(() -> new ResourceNotFoundException("Role", "Role Id", roleId));
		if (role != null) {
			roleDto.setName(role.getName());
			this.roleRepository.delete(role);
		}

		return roleDto;
	}

	@Override
	public List<RoleDtoInterface> getRoleByRoleId(Long roleId) {
		List<RoleDtoInterface> findByRoleId = this.roleRepository.findByRoleId(roleId, RoleDtoInterface.class);
		return findByRoleId;
	}

	@Override
	public List<RoleDtoInterface> getAllRoles() {
		List<RoleDtoInterface> roles = this.roleRepository.findByOrderByRoleIdDesc(RoleDtoInterface.class);
		return roles;
	}

	

}
