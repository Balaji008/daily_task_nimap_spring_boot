package com.demoproject.serviceimpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.demoproject.entities.Role;
import com.demoproject.entities.User;
import com.demoproject.entities.UserRole;
import com.demoproject.exceptions.ResourceNotFoundException;
import com.demoproject.payload.UserRoleDto;
import com.demoproject.payload.UserRoleDtoInterface;
import com.demoproject.repository.RoleRepository;
import com.demoproject.repository.UserRepository;
import com.demoproject.repository.UserRoleRepository;
import com.demoproject.serviceinterfaces.UserRoleServiceInterface;

@Service
public class UserRoleServiceImpl implements UserRoleServiceInterface {

	@Autowired
	private UserRoleRepository userRoleRepository;

	@Autowired
	private RoleRepository roleRepository;

	@Autowired
	private UserRepository userRepository;

	@Override
	public UserRoleDto addRoleToUser(UserRoleDto userRoleDto) {
		UserRole userRole = new UserRole();

		User user = this.userRepository.findById(userRoleDto.getUserId())
				.orElseThrow(() -> new ResourceNotFoundException("User", "User Id", userRoleDto.getUserId()));

		Role role = this.roleRepository.findById(userRoleDto.getRoleId())
				.orElseThrow(() -> new ResourceNotFoundException("Role", "Role Id", userRoleDto.getRoleId()));

		userRole.setUser(user);
		userRole.setRole(role);
		this.userRoleRepository.save(userRole);
		return userRoleDto;
	}

	@Override
	public UserRoleDto updateUserRole(UserRoleDto userRoleDto, Long Id) {
		UserRole userRole = this.userRoleRepository.findById(Id)
				.orElseThrow(() -> new ResourceNotFoundException("UserRole", "UserRole Id", Id));

		User user = this.userRepository.findById(userRoleDto.getUserId())
				.orElseThrow(() -> new ResourceNotFoundException("User", "User Id", userRoleDto.getUserId()));

		Role role = this.roleRepository.findById(userRoleDto.getRoleId())
				.orElseThrow(() -> new ResourceNotFoundException("Role", "Role Id", userRoleDto.getRoleId()));

		userRole.setUser(user);
		userRole.setRole(role);
		this.userRoleRepository.save(userRole);
		return userRoleDto;

	}

	@Override
	public List<UserRoleDtoInterface> getUserRoleByUserId(Long userId) {

		List<UserRoleDtoInterface> userRoles = this.userRoleRepository.findByUserId(userId, UserRoleDtoInterface.class);
		return userRoles;
	}

	@Override
	public List<UserRoleDtoInterface> getUserRoleByRoleId(Long roleId) {

		List<UserRoleDtoInterface> userRoles = this.userRoleRepository.findByRoleId(roleId, UserRoleDtoInterface.class);
		return userRoles;
	}

	@Override
	public List<UserRoleDtoInterface> getAllUserRoles() {
		List<UserRoleDtoInterface> userRoles = this.userRoleRepository
				.findByOrderByUserRoleIdDesc(UserRoleDtoInterface.class);
		return userRoles;
	}

	@Override
	public void deleteUserRole(Long Id) {

		this.userRoleRepository.deleteById(Id);
	}

}
