package com.demoproject.serviceimpl;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.demoproject.entities.Permission;
import com.demoproject.entities.Role;
import com.demoproject.entities.RolePermission;
import com.demoproject.exceptions.ResourceNotFoundException;
import com.demoproject.payload.IListRolePermissionDto;
import com.demoproject.payload.RolePermissionDto;
import com.demoproject.repository.PermissionRepository;
import com.demoproject.repository.RolePermissionRepository;
import com.demoproject.repository.RoleRepository;
import com.demoproject.serviceinterfaces.RolePermissionServiceInterface;

@Service
public class RolePermissionServiceImpl implements RolePermissionServiceInterface {

	@Autowired
	private RolePermissionRepository rolePermissionRepository;

	@Autowired
	private PermissionRepository permissionRepository;

	@Autowired
	private RoleRepository roleRepository;

	@Override
	public RolePermissionDto assignPermissionToRole(RolePermissionDto rolePermissionDto) {

		Permission permission = this.permissionRepository.findById(rolePermissionDto.getPermissionId())
				.orElseThrow(() -> new ResourceNotFoundException("Permission", "Permission id",
						rolePermissionDto.getPermissionId()));

		Role role = this.roleRepository.findById(rolePermissionDto.getRoleId())
				.orElseThrow(() -> new ResourceNotFoundException("Role", "Role Id", rolePermissionDto.getRoleId()));

		RolePermission rolePermission = new RolePermission();

		rolePermission.setPermission(permission);
		rolePermission.setRole(role);
		this.rolePermissionRepository.save(rolePermission);
		return rolePermissionDto;
	}

	@Override
	public RolePermissionDto updateRolePermission(RolePermissionDto rolePermissionDto, Long id) {
		RolePermission rolePermission = this.rolePermissionRepository.findById(id)
				.orElseThrow(() -> new ResourceNotFoundException("RolePermission", "RolePermission id", id));

		Permission permission = this.permissionRepository.findById(rolePermissionDto.getPermissionId())
				.orElseThrow(() -> new ResourceNotFoundException("Permission", "Permission id",
						rolePermissionDto.getPermissionId()));

		Role role = this.roleRepository.findById(rolePermissionDto.getRoleId())
				.orElseThrow(() -> new ResourceNotFoundException("Role", "Role Id", rolePermissionDto.getRoleId()));

		rolePermission.setRole(role);
		rolePermission.setPermission(permission);
		this.rolePermissionRepository.save(rolePermission);
		return rolePermissionDto;
	}

	@Override
	public void deleteRolePermission(Long id) {

		RolePermission rolePermission = this.rolePermissionRepository.findById(id)
				.orElseThrow(() -> new ResourceNotFoundException("RolePermission", "RolePermission id", id));

		this.rolePermissionRepository.delete(rolePermission);

	}

	@Override
	public List<IListRolePermissionDto> getAllRolePermissions() {
		List<IListRolePermissionDto> rolePermissions = this.rolePermissionRepository
				.findByOrderByIdDesc(IListRolePermissionDto.class);
		return rolePermissions;
	}

	@Override
	public ArrayList<String> getAllPermissionsByUserId(Long userId) {
		LinkedHashSet<String> permissions = this.rolePermissionRepository.findPermissionsByUserId(userId);
		ArrayList<String> permissions1 = new ArrayList<String>(permissions);
		return permissions1;
	}

}
