package com.demoproject.serviceimpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.demoproject.entities.Category;
import com.demoproject.entities.Post;
import com.demoproject.entities.User;
import com.demoproject.exceptions.ResourceNotFoundException;
import com.demoproject.payload.PostDto;
import com.demoproject.payload.PostDtoInterface;
import com.demoproject.payload.PostResponse;
import com.demoproject.repository.CategoryRepository;
import com.demoproject.repository.PostRepository;
import com.demoproject.repository.UserRepository;
import com.demoproject.serviceinterfaces.PostServiceInterfaces;

@Service
public class PostServiceImpl implements PostServiceInterfaces {

	@Autowired
	private PostRepository postRepository;

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private CategoryRepository categoryRepository;

	@Override
	public PostDto createPost(PostDto postDto) {

		Post post = new Post();

		User user = this.userRepository.findById(postDto.getUserId())
				.orElseThrow(() -> new ResourceNotFoundException("User", "userId",postDto.getUserId()));

		Category category = this.categoryRepository.findById(postDto.getCategoryId())
				.orElseThrow(() -> new ResourceNotFoundException("Category", "categoryId", postDto.getCategoryId()));

		post.setTitle(postDto.getTitle());
		post.setContent(postDto.getContent());
		post.setUser(user);
		post.setCategory(category);
		post.setImageName(postDto.getImageName());
		this.postRepository.save(post);
		return postDto;
	}

	@Override
	public PostDto updatePost(PostDto postDto, Long postId) {

		Post post = this.postRepository.findById(postId)
				.orElseThrow(() -> new ResourceNotFoundException("Post", "Post Id", postId));

		User user = this.userRepository.findById(postDto.getUserId())
				.orElseThrow(() -> new ResourceNotFoundException("User", "userId", postDto.getUserId()));

		Category category = this.categoryRepository.findById(postDto.getCategoryId())
				.orElseThrow(() -> new ResourceNotFoundException("Category", "categoryId", postDto.getCategoryId()));
		post.setTitle(postDto.getTitle());
		post.setContent(postDto.getContent());
		post.setImageName(postDto.getImageName());
		post.setUser(user);
		post.setCategory(category);
		this.postRepository.save(post);
		return postDto;
	}

	@Override
	public PostDto deletePost(Long postId) {

		Post post = null;
		PostDto postDto = new PostDto();
		post = this.postRepository.findById(postId)
				.orElseThrow(() -> new ResourceNotFoundException("Post", "Post Id", postId));

		if (post != null) {
			postDto.setContent(post.getContent());
			postDto.setTitle(post.getTitle());
			postDto.setImageName(post.getImageName());
			this.postRepository.delete(post);
		}
		return postDto;
	}

	@Override
	public PostResponse getAllPosts(Integer pageNumber, Integer pageSize, String sortBy) {

		Pageable p = PageRequest.of(pageNumber, pageSize, Sort.by(sortBy));
		Page<PostDtoInterface> pagePost = this.postRepository.findByOrderByPostIdDesc(p, PostDtoInterface.class);

		List<PostDtoInterface> posts = pagePost.getContent();

		PostResponse postResponse = new PostResponse();

		postResponse.setContent(posts);
		postResponse.setPageNumber(pagePost.getNumber());
		postResponse.setPageSize(pagePost.getSize());
		postResponse.setTotalElements(pagePost.getTotalElements());
		postResponse.setTotalPages(pagePost.getTotalPages());
		postResponse.setLastPage(pagePost.isLast());
		return postResponse;
	}

	@Override
	public List<PostDtoInterface> getPostById(Long postId) {

		List<PostDtoInterface> findByPostId = this.postRepository.findByPostId(postId, PostDtoInterface.class);

		return findByPostId;
	}

	@Override
	public List<PostDtoInterface> getPostsByCategory(Long categoryId) {

		Category category = this.categoryRepository.findById(categoryId)
				.orElseThrow(() -> new ResourceNotFoundException("Category", "Category Id", categoryId));

		List<PostDtoInterface> posts = this.postRepository.findByCategory(category, PostDtoInterface.class);

		return posts;
	}

	@Override
	public List<PostDtoInterface> getPostsByUser(Long userId) {

		User user = this.userRepository.findById(userId)
				.orElseThrow(() -> new ResourceNotFoundException("User", "User Id", userId));

		List<PostDtoInterface> posts = this.postRepository.findByUser(user, PostDtoInterface.class);
		return posts;
	}

	@Override
	public List<PostDtoInterface> searchPosts(String keyword) {

		List<PostDtoInterface> post = this.postRepository.findByTitleContaining(keyword, PostDtoInterface.class);
		return post;
	}

}
