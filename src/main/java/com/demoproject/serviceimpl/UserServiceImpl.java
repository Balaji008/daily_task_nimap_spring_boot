package com.demoproject.serviceimpl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.demoproject.entities.User;
import com.demoproject.exceptions.ResourceNotFoundException;
import com.demoproject.payload.UserDto;
import com.demoproject.payload.UserDtoInterface;
import com.demoproject.repository.UserRepository;
import com.demoproject.serviceinterfaces.RolePermissionServiceInterface;
import com.demoproject.serviceinterfaces.UserServiceInterface;

@Service
public class UserServiceImpl implements UserServiceInterface, UserDetailsService {

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private PasswordEncoder passwordEncoder;

	@Autowired
	private RolePermissionServiceInterface rolePermissionService;

	@Override
	public UserDto createUser(UserDto userDto) {

		User user = new User();
		user.setAbout(userDto.getAbout());
		user.setName(userDto.getName());
		user.setEmail(userDto.getEmail());
		user.setPassword(this.passwordEncoder.encode(userDto.getPassword()));
		this.userRepository.save(user);
		return userDto;
	}

	@Override
	public UserDto updateUser(UserDto userDto, Long userId) {

		User user = this.userRepository.findById(userId)
				.orElseThrow(() -> new ResourceNotFoundException("User", "id", userId));

		user.setName(userDto.getName());
		user.setEmail(userDto.getEmail());
		user.setPassword(this.passwordEncoder.encode(userDto.getPassword()));
		user.setAbout(userDto.getAbout());

		this.userRepository.save(user);
		return userDto;
	}

	@Override
	public List<UserDtoInterface> getUserById(Long userId) {
		List<UserDtoInterface> list;

		list = this.userRepository.findById(userId, UserDtoInterface.class);

		return list;
	}

	@Override
	public List<UserDtoInterface> getAllUsers() {
		List<UserDtoInterface> list;

		list = this.userRepository.findByOrderByIdDesc(UserDtoInterface.class);

		return list;
	}

	@Override
	public void deleteUser(Long userId) {

		User user = null;

		user = this.userRepository.findById(userId)
				.orElseThrow(() -> new ResourceNotFoundException("User", "UserId", userId));

		if (user != null)
			this.userRepository.delete(user);
	}

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

		User user = this.userRepository.findByEmail(username);

		return new org.springframework.security.core.userdetails.User(user.getEmail(), user.getPassword(),
				getAuthority(user));
	}

	public Boolean comparePassword(String password, String hashPassword) {

		return passwordEncoder.matches(password, hashPassword);

	}

	private ArrayList<SimpleGrantedAuthority> getAuthority(User user) {
		ArrayList<SimpleGrantedAuthority> authorities = new ArrayList<>();

		ArrayList<String> permissions = this.rolePermissionService.getAllPermissionsByUserId(user.getId());

		permissions.forEach(e -> {
			authorities.add(new SimpleGrantedAuthority("ROLE_" + e));

		});

		Collections.sort(authorities, Comparator.comparing(SimpleGrantedAuthority::getAuthority));

		return authorities;

	}

	@Override
	public ArrayList<String> getAllPermissionsByUserId(Long userId) {
		ArrayList<String> permissions = this.rolePermissionService.getAllPermissionsByUserId(userId);
		Collections.sort(permissions, Collections.reverseOrder());
		return permissions;
	}

}
