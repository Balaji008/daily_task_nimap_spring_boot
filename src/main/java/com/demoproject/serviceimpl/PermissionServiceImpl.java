package com.demoproject.serviceimpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.demoproject.entities.Permission;
import com.demoproject.exceptions.ResourceNotFoundException;
import com.demoproject.payload.PermissionDto;
import com.demoproject.payload.PermissionDtoInterface;
import com.demoproject.repository.PermissionRepository;
import com.demoproject.serviceinterfaces.PermissionServiceInterface;

@Service
public class PermissionServiceImpl implements PermissionServiceInterface {

	@Autowired
	private PermissionRepository permissionRepository;

	@Override
	public PermissionDto createPermission(PermissionDto permissionDto) {

		Permission permission = new Permission();

		permission.setActionName(permissionDto.getActionName());

		this.permissionRepository.save(permission);
		return permissionDto;
	}

	@Override
	public List<PermissionDtoInterface> getAllPermissions() {
		List<PermissionDtoInterface> permissions = this.permissionRepository
				.findByOrderByIdDesc(PermissionDtoInterface.class);
		return permissions;
	}

	@Override
	public List<PermissionDtoInterface> getPermissionById(Long id) {

		List<PermissionDtoInterface> permission = null;
		permission = this.permissionRepository.findById(id, PermissionDtoInterface.class);

		if (permission == null) {
			throw new ResourceNotFoundException("Permission", "Permission Id", id);
		} else
			return permission;
	}

	@Override
	public PermissionDto updatePermission(PermissionDto permissionDto, Long id) {
		Permission permission = this.permissionRepository.findById(id)
				.orElseThrow(() -> new ResourceNotFoundException("Permission", "Permission Id", id));

		permission.setActionName(permissionDto.getActionName());

		this.permissionRepository.save(permission);
		return permissionDto;
	}

	@Override
	public void deletePermission(Long id) {
		Permission permission = this.permissionRepository.findById(id)
				.orElseThrow(() -> new ResourceNotFoundException("Permission", "Permission id", id));
		this.permissionRepository.delete(permission);
	}

}
