package com.demoproject.serviceimpl;

import java.time.Instant;

import org.apache.tomcat.util.codec.binary.Base64;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
public class SamlService {

//	@Value("${saml.endpoint}")
	private String samlEndpoint = "https://samltoolkit.azurewebsites.net/SAML/Login/11310";

	public void sendSamlRequest() {
		RestTemplate restTemplate = new RestTemplate();
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);

		String samlRequest = "<samlp:AuthnRequest xmlns:samlp=\"urn:oasis:names:tc:SAML:2.0:protocol\" "
				+ "xmlns:saml=\"urn:oasis:names:tc:SAML:2.0:assertion\" " + "ID=\"" + Instant.now().toEpochMilli()
				+ "\" " + "Version=\"2.0\" " + "IssueInstant=\"" + Instant.now() + "\" "
				+ "ProtocolBinding=\"urn:oasis:names:tc:SAML:2.0:bindings:HTTP-POST\" "
				+ "AssertionConsumerServiceURL=\"https://login.microsoftonline.com/1ebc9497-2161-472a-84b4-ea64ae60db97/saml2\">"
				+ "<saml:Issuer>https://login.microsoftonline.com/1ebc9497-2161-472a-84b4-ea64ae60db97/saml2</saml:Issuer>"
				+ "<samlp:NameIDPolicy Format=\"urn:oasis:names:tc:SAML:1.1:nameid-format:unspecified\"/>"
				+ "</samlp:AuthnRequest>";

		byte[] bytes = samlRequest.getBytes();
		byte[] encodedBytes = Base64.encodeBase64(bytes);
		String encodedSamlRequest = new String(encodedBytes);

		String body = "SAMLRequest=" + encodedSamlRequest;
		System.err.println(encodedSamlRequest);
		HttpEntity<String> request = new HttpEntity<>(body, headers);
		ResponseEntity<String> response = restTemplate.exchange(samlEndpoint, HttpMethod.GET, request, String.class);

		System.out.println(response.getBody());
		// process response here
	}
}
