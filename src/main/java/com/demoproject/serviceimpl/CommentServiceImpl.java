package com.demoproject.serviceimpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.demoproject.entities.Comment;
import com.demoproject.entities.Post;
import com.demoproject.entities.User;
import com.demoproject.exceptions.ResourceNotFoundException;
import com.demoproject.payload.CommentDto;
import com.demoproject.repository.CommentRepository;
import com.demoproject.repository.PostRepository;
import com.demoproject.repository.UserRepository;
import com.demoproject.serviceinterfaces.CommentServiceInterface;

@Service
public class CommentServiceImpl implements CommentServiceInterface {

	@Autowired
	private PostRepository postRepository;

	@Autowired
	private CommentRepository commentRepository;

	@Autowired
	private UserRepository userRepository;

	@Override
	public CommentDto createComment(CommentDto commentDto, Long userId, Long postId) {

		Post post = this.postRepository.findById(postId)
				.orElseThrow(() -> new ResourceNotFoundException("Post", "Post Id", postId));

		User user = this.userRepository.findById(userId)
				.orElseThrow(() -> new ResourceNotFoundException("User", "User Id", userId));

		Comment comment = new Comment();

		comment.setContent(commentDto.getContent());
		comment.setPost(post);
		comment.setUser(user);
		this.commentRepository.save(comment);
		return commentDto;
	}

	@Override
	public void deleteComment(Long commentId) {

		Comment comment = this.commentRepository.findById(commentId)
				.orElseThrow(() -> new ResourceNotFoundException("Comment", "Comment Id", commentId));

		this.commentRepository.delete(comment);

	}

}
