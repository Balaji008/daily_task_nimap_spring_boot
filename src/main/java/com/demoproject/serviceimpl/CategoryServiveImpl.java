package com.demoproject.serviceimpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.demoproject.entities.Category;
import com.demoproject.exceptions.ResourceNotFoundException;
import com.demoproject.payload.CategoryDto;
import com.demoproject.payload.CategoryDtoInterface;
import com.demoproject.repository.CategoryRepository;
import com.demoproject.serviceinterfaces.CategoryServiceInterface;

@Service
public class CategoryServiveImpl implements CategoryServiceInterface {

	@Autowired
	private CategoryRepository categoryRepository;

	@Override
	public CategoryDto createCategory(CategoryDto categoryDto) {

		Category category = new Category();
		category.setCategoryTitle(categoryDto.getCategoryTitle());
		category.setCategoryDescription(categoryDto.getCategoryDescription());
		this.categoryRepository.save(category);
		return categoryDto;
	}

	@Override
	public CategoryDto updateCategory(CategoryDto categoryDto, Long categoryId) {
		Category category = this.categoryRepository.findById(categoryId)
				.orElseThrow(() -> new ResourceNotFoundException("Category", "Id", categoryId));

		category.setCategoryTitle(categoryDto.getCategoryTitle());
		category.setCategoryDescription(categoryDto.getCategoryDescription());
		this.categoryRepository.save(category);

		return categoryDto;
	}

	@Override
	public CategoryDto deleteCategory(Long categoryId) {

		Category category = null;
		CategoryDto category2 = new CategoryDto();

		category = this.categoryRepository.findById(categoryId)
				.orElseThrow(() -> new ResourceNotFoundException("Category", "Id", categoryId));

		if (category != null) {
			category2.setCategoryTitle(category.getCategoryTitle());
			category2.setCategoryDescription(category.getCategoryDescription());
			this.categoryRepository.delete(category);
		}
		return category2;
	}

	@Override
	public List<CategoryDtoInterface> getCategoryById(Long categoryId) {

		List<CategoryDtoInterface> list = this.categoryRepository.findByCategoryId(categoryId,
				CategoryDtoInterface.class);
		return list;
	}

	@Override
	public List<CategoryDtoInterface> getAllCategories() {
		List<CategoryDtoInterface> list = this.categoryRepository
				.findByOrderByCategoryIdDesc(CategoryDtoInterface.class);

		return list;
	}

}
