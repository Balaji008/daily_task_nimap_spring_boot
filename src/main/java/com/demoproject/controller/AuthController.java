package com.demoproject.controller;

import java.util.ArrayList;
import java.util.UUID;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.demoproject.entities.User;
import com.demoproject.exceptions.ResourceNotFoundException;
import com.demoproject.payload.ErrorResponseDto;
import com.demoproject.payload.JwtAuthenticationRequest;
import com.demoproject.payload.JwtAuthenticationResponse;
import com.demoproject.payload.JwtEmailRequest;
import com.demoproject.payload.JwtTokenRequest;
import com.demoproject.payload.SuccessResponseDto;
import com.demoproject.payload.UserDto;
import com.demoproject.repository.UserRepository;
import com.demoproject.security.JwtTokenHelper;
import com.demoproject.serviceinterfaces.UserServiceInterface;

@RestController
@RequestMapping("/auth")
public class AuthController {

	@Autowired
	private JwtTokenHelper jwtTokenHelper;

//	@Autowired
//	private UserDetailsService userDetailsService;
//
//	@Autowired
//	private AuthenticationManager authenticationManager;

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private PasswordEncoder passwordEncoder;

	@Autowired
	private UserServiceInterface userService;

	private User user = null;

	@PostMapping("/login")
	public ResponseEntity<?> createToken(@RequestBody JwtAuthenticationRequest request) throws Exception {
		try {
			User user = null;
			if (this.userRepository.findByEmail(request.getUsername()) != null) {
				user = this.userRepository.findByEmailIgnoreCase(request.getUsername());
			} else {
				throw new ResourceNotFoundException("User", "Username", request.getUsername());
			}
			if (this.userService.comparePassword(request.getPassword(), user.getPassword())) {

				final UserDetails userDetails = this.userService.loadUserByUsername(request.getUsername());

				final String token = this.jwtTokenHelper.generateToken(userDetails);

				final String refreshToken = this.jwtTokenHelper.refreshToken(token, userDetails);

				ArrayList<String> permissions = this.userService.getAllPermissionsByUserId(user.getId());
				JwtAuthenticationResponse response = new JwtAuthenticationResponse();
				response.setAccessToken(token);
				response.setRefreshToken(refreshToken);
				response.setPermissions(permissions);
				return new ResponseEntity<>(new SuccessResponseDto("Login Successful", "token", response),
						HttpStatus.OK);
			}

			else {
				ErrorResponseDto error = new ErrorResponseDto();
				error.setKey("Access Denied");
				error.setMessage("Invalid Email or Password");
				return new ResponseEntity<>(error, HttpStatus.BAD_REQUEST);
			}
		} catch (Exception e) {
			ErrorResponseDto error = new ErrorResponseDto();
			error.setKey("Access Denied");
			error.setMessage("Invalid Email or Password");
			return new ResponseEntity<>(error, HttpStatus.BAD_REQUEST);
		}
	}

	@PostMapping("/register")
	public ResponseEntity<?> registeruser(@Valid @RequestBody UserDto userDto) {

		// add check for email exists in DB
		if (userRepository.existsByEmail(userDto.getEmail())) {
			return new ResponseEntity<>("Email is already taken!", HttpStatus.BAD_REQUEST);
		}

		// create user object
		User user = new User();
		user.setName(userDto.getName());
		user.setEmail(userDto.getEmail());
		String regex = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&-+=()])(?=\\S+$).{8,20}$";
		if (userDto.getPassword().matches(regex)) {
			user.setPassword(this.passwordEncoder.encode(userDto.getPassword()));
			userRepository.save(user);
			return new ResponseEntity<>("User registered successfully", HttpStatus.OK);
		} else {
			return new ResponseEntity<>("Invalid Password", HttpStatus.BAD_REQUEST);
		}
	}

	@PostMapping("/forgotpassword")
	public ResponseEntity<?> forgotPassword(@RequestBody JwtEmailRequest request) {
		User user = null;
		if (this.userRepository.findByEmail(request.getUsername()) != null) {
			user = this.userRepository.findByEmail(request.getUsername());
		} else {
			throw new ResourceNotFoundException("User", "Username", request.getUsername());
		}

		UUID uuid = UUID.randomUUID();

		this.user = user;
		this.user.setUuid(uuid);
		this.userRepository.save(this.user);
		return new ResponseEntity<>(uuid, HttpStatus.OK);

	}

	@PostMapping("/setpassword")
	public ResponseEntity<?> setPassword(@RequestBody JwtTokenRequest request) {

		if (this.user.getUuid().equals(request.getUuid())) {
			user.setPassword(this.passwordEncoder.encode(request.getPassword()));
			this.userRepository.save(user);
			return new ResponseEntity<>("Password updated successfully", HttpStatus.OK);
		}
		return new ResponseEntity<>("Invalid JWT token", HttpStatus.OK);
	}

	@PostMapping("/refreshToken")
	public ResponseEntity<?> refreshAndGetAuthenticationToken(@RequestParam(defaultValue = "") String refreshToken)
			throws Exception {
		try {
			String email = jwtTokenHelper.getUsernameFromToken(refreshToken);
			User user = this.userRepository.findByEmailIgnoreCase(email);
			if (user == null) {
				return new ResponseEntity<>(new ErrorResponseDto("Invalid user"), HttpStatus.UNAUTHORIZED);
			}
			final UserDetails userDetails = this.userService.loadUserByUsername(email);
			if (jwtTokenHelper.canTokenBeRefreshed(refreshToken)
					&& jwtTokenHelper.validateToken(refreshToken, userDetails)
					&& jwtTokenHelper.getTokenType(refreshToken).equalsIgnoreCase("refresh")) {

				String newAccessToken = jwtTokenHelper.generateToken(userDetails);

				return new ResponseEntity<>(new SuccessResponseDto("Access token", "Successfull", newAccessToken),
						HttpStatus.OK);
			} else {
				return new ResponseEntity<>(new ErrorResponseDto("Invalid token"), HttpStatus.UNAUTHORIZED);

			}
		} catch (Exception e) {
			return new ResponseEntity<>(new ErrorResponseDto("Invalid token"), HttpStatus.UNAUTHORIZED);
		}

	}

//	public ResponseEntity<?> createRefreshToken(@RequestParam (defaultValue = "") String refreshToken){
//		
//		String email = this.jwtTokenHelper.getUsernameFromToken(refreshToken);
//		
//	}

}