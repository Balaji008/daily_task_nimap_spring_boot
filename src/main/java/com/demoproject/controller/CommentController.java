package com.demoproject.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.demoproject.payload.APIResponse;
import com.demoproject.payload.CommentDto;
import com.demoproject.serviceinterfaces.CommentServiceInterface;

@RestController
@RequestMapping("/comments")
public class CommentController {

	@Autowired
	private CommentServiceInterface commentService;

	@PostMapping("/user/{userId}/post/{postId}")
	public ResponseEntity<APIResponse> createComment(@RequestBody CommentDto commentDto, @PathVariable Long userId,
			@PathVariable Long postId) {
		CommentDto createdComment = this.commentService.createComment(commentDto, userId, postId);

		return new ResponseEntity<APIResponse>(new APIResponse("Comment added successfully", createdComment),
				HttpStatus.CREATED);
	}

	@DeleteMapping("/{commentId}")
	public ResponseEntity<APIResponse> deleteComment(@PathVariable Long commentId) {
		this.commentService.deleteComment(commentId);

		return new ResponseEntity<APIResponse>(new APIResponse("Comment deleted Successfully"), HttpStatus.OK);
	}
}
