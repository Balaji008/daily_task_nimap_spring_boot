package com.demoproject.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.demoproject.payload.APIResponse;
import com.demoproject.payload.IListRolePermissionDto;
import com.demoproject.payload.RolePermissionDto;
import com.demoproject.serviceinterfaces.RolePermissionServiceInterface;

@RestController
@RequestMapping("/rolepermission")
public class RolePermissionController {

	@Autowired
	private RolePermissionServiceInterface rolePermissionService;

	@PostMapping
	public ResponseEntity<?> assignPermissionToRole(@RequestBody RolePermissionDto rolePermissionDto) {
		RolePermissionDto createdRolePermission = this.rolePermissionService.assignPermissionToRole(rolePermissionDto);

		return new ResponseEntity<APIResponse>(
				new APIResponse("Permission added to role successfully", createdRolePermission), HttpStatus.OK);
	}

	@PutMapping("/{id}")
	public ResponseEntity<?> updateRolePermission(@RequestBody RolePermissionDto rolePermissionDto,
			@PathVariable Long id) {

		RolePermissionDto updatedRolePermission = this.rolePermissionService.updateRolePermission(rolePermissionDto,
				id);

		return new ResponseEntity<APIResponse>(
				new APIResponse("RolePermission updated successfully", updatedRolePermission), HttpStatus.OK);
	}

	@DeleteMapping("/{id}")
	public ResponseEntity<?> deleteRolePermission(@PathVariable Long id) {
		this.rolePermissionService.deleteRolePermission(id);

		return new ResponseEntity<APIResponse>(new APIResponse("RolePermission deleted successfully"), HttpStatus.OK);
	}

	@GetMapping
	public ResponseEntity<?> getAllRolePermissions() {
		List<IListRolePermissionDto> allRolePermissions = this.rolePermissionService.getAllRolePermissions();
		return ResponseEntity.ok(allRolePermissions);
	}
}
