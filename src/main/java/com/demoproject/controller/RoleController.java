package com.demoproject.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.demoproject.payload.APIResponse;
import com.demoproject.payload.RoleDto;
import com.demoproject.payload.RoleDtoInterface;
import com.demoproject.serviceinterfaces.RoleServiceInterface;

@RestController
@RequestMapping("/roles")
public class RoleController {

	@Autowired
	private RoleServiceInterface roleService;

	@PostMapping
	public ResponseEntity<?> createRole(@RequestBody RoleDto roleDto) {
		RoleDto createdRole = this.roleService.addRole(roleDto);

		return new ResponseEntity<APIResponse>(new APIResponse("Role created successfully", createdRole),
				HttpStatus.CREATED);
	}

	@PutMapping("/{roleId}")
	public ResponseEntity<?> updateRole(@RequestBody RoleDto roleDto, @PathVariable Long roleId) {
		RoleDto updatedRole = this.roleService.updateRole(roleDto, roleId);
		return new ResponseEntity<APIResponse>(new APIResponse("Role updated successfully", updatedRole),
				HttpStatus.OK);
	}

	@GetMapping("/{roleId}")
	public ResponseEntity<?> getRole(@PathVariable Long roleId) {
		List<RoleDtoInterface> role = this.roleService.getRoleByRoleId(roleId);

		return new ResponseEntity<APIResponse>(new APIResponse("Role updated successfully", role), HttpStatus.OK);
	}

	@GetMapping
	public ResponseEntity<?> getAllRoles() {
		List<RoleDtoInterface> roles = this.roleService.getAllRoles();

		return new ResponseEntity<APIResponse>(new APIResponse("Role updated successfully", roles), HttpStatus.OK);
	}

	@DeleteMapping("/{roleId}")
	public ResponseEntity<?> deleteRole(@PathVariable Long roleId) {
		RoleDto role = this.roleService.deleteRole(roleId);

		return new ResponseEntity<APIResponse>(new APIResponse("Role deleted successfully", role), HttpStatus.OK);
	}
}
