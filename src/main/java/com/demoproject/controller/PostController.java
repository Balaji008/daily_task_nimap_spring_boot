package com.demoproject.controller;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.hibernate.engine.jdbc.StreamUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.demoproject.config.AppConstants;
import com.demoproject.entities.Post;
import com.demoproject.exceptions.ResourceNotFoundException;
import com.demoproject.payload.APIResponse;
import com.demoproject.payload.PostDto;
import com.demoproject.payload.PostDtoInterface;
import com.demoproject.payload.PostResponse;
import com.demoproject.repository.PostRepository;
import com.demoproject.serviceimpl.FileServiceImpl;
import com.demoproject.serviceinterfaces.PostServiceInterfaces;

@RestController
@RequestMapping("/posts")
public class PostController {

	@Autowired
	private PostServiceInterfaces postService;

	@Autowired
	private FileServiceImpl fileservice;

	@Autowired
	private PostRepository postRepository;

	@Value("${project.image}")
	private String path;

	@PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> createPost(@RequestBody PostDto postDto) {
		// System.err.println(postDto.getImageName());
		PostDto createdPostDto = this.postService.createPost(postDto);

		return new ResponseEntity<APIResponse>(new APIResponse("Post created successfully", createdPostDto),
				HttpStatus.CREATED);
	}

	@GetMapping("/category/{categoryId}")
	public ResponseEntity<List<PostDtoInterface>> getPostsByCategory(@PathVariable("categoryId") Long categoryId) {
		List<PostDtoInterface> postsByCategory = this.postService.getPostsByCategory(categoryId);

		return ResponseEntity.ok(postsByCategory);
	}

	@GetMapping("/user/{userId}")
	public ResponseEntity<List<PostDtoInterface>> getPostsByUser(@PathVariable("userId") Long userId) {

		List<PostDtoInterface> postsByUser = this.postService.getPostsByUser(userId);

		return ResponseEntity.ok(postsByUser);
	}

	@GetMapping("/{postId}")
	public ResponseEntity<List<PostDtoInterface>> getPostById(@PathVariable("postId") Long postId) {
		List<PostDtoInterface> postById = this.postService.getPostById(postId);

		return ResponseEntity.ok(postById);
	}

	@GetMapping
	public ResponseEntity<PostResponse> getAllPosts(
			@RequestParam(value = "pageNumber", defaultValue = AppConstants.PAGE_NUMBER, required = false) Integer pageNumber,
			@RequestParam(value = "pageSize", defaultValue = AppConstants.PAGE_SIZE, required = false) Integer pageSize,
			@RequestParam(value = "sortBy", defaultValue = AppConstants.SORT_BY, required = false) String sortBy) {
		PostResponse Posts = this.postService.getAllPosts(pageNumber, pageSize, sortBy);

		return ResponseEntity.ok(Posts);
	}

	@PutMapping("/{postId}")
	public ResponseEntity<APIResponse> updatePost(@RequestBody PostDto postDto, @PathVariable Long postId) {

		PostDto updatedPost = this.postService.updatePost(postDto, postId);

		return new ResponseEntity<APIResponse>(new APIResponse("Post updated successfully", updatedPost),
				HttpStatus.OK);
	}

	@DeleteMapping("/{postId}")
	public ResponseEntity<APIResponse> deletePost(@PathVariable Long postId) {
		PostDto deletedPost = this.postService.deletePost(postId);

		return new ResponseEntity<APIResponse>(new APIResponse("Post deleted successfully", deletedPost),
				HttpStatus.OK);
	}

	@GetMapping("/search/{keyword}")
	public ResponseEntity<List<PostDtoInterface>> searchByPostTitle(@PathVariable String keyword) {

		List<PostDtoInterface> searchedPosts = this.postService.searchPosts(keyword);

		return ResponseEntity.ok(searchedPosts);
	}

	// Post Image Upload
	@PostMapping("/image/upload/{postId}")
	public ResponseEntity<APIResponse> uploadImage(@RequestParam(name = "file") MultipartFile image,
			@PathVariable Long postId) throws IOException {
		Post post = this.postRepository.findById(postId)
				.orElseThrow(() -> new ResourceNotFoundException("Post", "Post Id", postId));
		String fileName = this.fileservice.uploadImage(path, image);

		post.setImageName(fileName);

		this.postRepository.save(post);

		List<PostDtoInterface> list = this.postRepository.findByPostId(postId, PostDtoInterface.class);

		return new ResponseEntity<APIResponse>(new APIResponse("Image uploaded successfullly", list), HttpStatus.OK);
	}

	@GetMapping(value = "/image/{imageName}", produces = MediaType.IMAGE_JPEG_VALUE)
	public void downloadImage(@PathVariable("imageName") String imageName, HttpServletResponse response)
			throws IOException {
		InputStream resource = this.fileservice.getResource(path, imageName);
		response.setContentType(MediaType.IMAGE_JPEG_VALUE);
		StreamUtils.copy(resource, response.getOutputStream());
	}

}
