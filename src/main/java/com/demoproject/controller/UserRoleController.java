package com.demoproject.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.demoproject.payload.APIResponse;
import com.demoproject.payload.UserRoleDto;
import com.demoproject.payload.UserRoleDtoInterface;
import com.demoproject.serviceinterfaces.UserRoleServiceInterface;

@RestController
@RequestMapping("/userRole")
public class UserRoleController {

	@Autowired
	private UserRoleServiceInterface userRoleService;

	@PostMapping
	public ResponseEntity<?> assignRoleToUser(@RequestBody UserRoleDto userRoleDto) {
		UserRoleDto createdUserRole = this.userRoleService.addRoleToUser(userRoleDto);

		return new ResponseEntity<APIResponse>(new APIResponse("Role assigned to user successfully", createdUserRole),
				HttpStatus.OK);
	}

	@PutMapping("/{id}")
	public ResponseEntity<?> updateUserRole(@RequestBody UserRoleDto userRoleDto, @PathVariable Long id) {
		UserRoleDto updatedUserRole = this.userRoleService.updateUserRole(userRoleDto, id);

		return new ResponseEntity<APIResponse>(new APIResponse("User role updated successfully", updatedUserRole),
				HttpStatus.OK);
	}

	@GetMapping
	public ResponseEntity<?> getAllUserRoles() {
		List<UserRoleDtoInterface> allUserRoles = this.userRoleService.getAllUserRoles();

		return ResponseEntity.ok(allUserRoles);
	}

	@GetMapping("/user/{userId}")
	public ResponseEntity<?> getUserRolesByUserId(@PathVariable Long userId) {
		List<UserRoleDtoInterface> roles = this.userRoleService.getUserRoleByUserId(userId);
		return ResponseEntity.ok(roles);
	}

	@GetMapping("/role/{roleId}")
	public ResponseEntity<?> getUserRolesByRoleId(@PathVariable Long roleId) {
		List<UserRoleDtoInterface> roles = this.userRoleService.getUserRoleByRoleId(roleId);
		return ResponseEntity.ok(roles);
	}

	@DeleteMapping("/{id}")
	public ResponseEntity<?> deleteUserRole(@PathVariable Long id) {
		this.userRoleService.deleteUserRole(id);

		return new ResponseEntity<APIResponse>(new APIResponse("User role deleted successfully"), HttpStatus.OK);
	}
}
