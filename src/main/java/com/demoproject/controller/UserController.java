package com.demoproject.controller;

import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.demoproject.payload.UserDto;
import com.demoproject.payload.UserDtoInterface;
import com.demoproject.serviceinterfaces.UserServiceInterface;

@RestController
@RequestMapping("/users")
public class UserController {

	@Autowired
	private UserServiceInterface userService;

//	@PostMapping
//	public ResponseEntity<UserDto> createUser(@Valid @RequestBody UserDto userDto) {
//
//		UserDto createdUserDto = this.userservice.createUser(userDto);
//
//		return new ResponseEntity<UserDto>(createdUserDto, HttpStatus.CREATED);
//	}

	@PutMapping("/{userId}")
	public ResponseEntity<UserDto> updateUser(@Valid @RequestBody UserDto userDto,
			@PathVariable("userId") Long userId) {
		UserDto updatedUser = this.userService.updateUser(userDto, userId);
		return ResponseEntity.ok(updatedUser);
	}

	@DeleteMapping("/{userId}")
	public ResponseEntity<?> deleteUser(@PathVariable("userId") Long userId) {
		this.userService.deleteUser(userId);

		return new ResponseEntity<Object>(Map.of("Massage", "User Deleted Successfully"), HttpStatus.OK);
	}

	@GetMapping
	public ResponseEntity<List<UserDtoInterface>> getAllUsers() {

		return ResponseEntity.ok(this.userService.getAllUsers());
	}

	@GetMapping("/{userId}")
	public ResponseEntity<List<UserDtoInterface>> getUserByUserId(@PathVariable("userId") Long UserId) {

		return ResponseEntity.ok(this.userService.getUserById(UserId));
	}

}
