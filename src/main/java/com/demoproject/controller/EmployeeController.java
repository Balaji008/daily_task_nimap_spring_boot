package com.demoproject.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.demoproject.entities.Employee;
import com.demoproject.serviceimpl.EmployeeService;

@RestController
@RequestMapping("employees")
public class EmployeeController {

	@Autowired
	private EmployeeService employeeservice;

	@GetMapping
	public ResponseEntity<List<Employee>> getAllEmployees() {
		List<Employee> list = this.employeeservice.getAllEmployees();
		if (list.isEmpty()) {
			return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
		}
		return ResponseEntity.status(HttpStatus.OK).body(list);
	}

	@GetMapping(value = "/{empid}")
	public ResponseEntity<Employee> getEmployeeById(@PathVariable("empid") int id) {
		Employee emp = null;
		emp = this.employeeservice.getEmployeeById(id);

		if (emp == null) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
		}
		return ResponseEntity.status(HttpStatus.OK).body(emp);
	}

	@PostMapping
	public ResponseEntity<Employee> addEmployee(@RequestBody Employee emp) {
		Employee employee = null;
		try {
			employee = this.employeeservice.addEmployee(emp);
			return ResponseEntity.of(Optional.of(employee));
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
		}
	}

	@DeleteMapping(value = "/{empid}")
	public ResponseEntity<?> deleteEmployeeByID(@PathVariable("empid") int id) {
		try {
			this.employeeservice.deleteEmployeeById(id);
			return new ResponseEntity<>(HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	@PutMapping(value = "/{empid}")
	public ResponseEntity<Employee> updateEmployeeById(@RequestBody Employee emp, @PathVariable("empid") int id) {
		Employee result = null;
		try {
			result = this.employeeservice.updateEmployeById(emp, id);
			return ResponseEntity.status(HttpStatus.OK).body(result);
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();

		}
	}

}
