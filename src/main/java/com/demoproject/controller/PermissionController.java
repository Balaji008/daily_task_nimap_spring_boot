package com.demoproject.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.demoproject.payload.APIResponse;
import com.demoproject.payload.PermissionDto;
import com.demoproject.payload.PermissionDtoInterface;
import com.demoproject.serviceimpl.PermissionServiceImpl;

@RestController
@RequestMapping("/permissions")
public class PermissionController {

	@Autowired
	private PermissionServiceImpl permissionService;

	@PostMapping
	public ResponseEntity<?> createPermission(@RequestBody PermissionDto permissionDto) {
		PermissionDto createdPermission = this.permissionService.createPermission(permissionDto);
		return new ResponseEntity<APIResponse>(new APIResponse("Permission added successfully", createdPermission),
				HttpStatus.OK);
	}

	@GetMapping
	public ResponseEntity<?> getAllPermissions() {

		List<PermissionDtoInterface> permissions = this.permissionService.getAllPermissions();
		return ResponseEntity.ok(permissions);
	}

	@GetMapping("/{id}")
	public ResponseEntity<?> getPermissionById(@PathVariable Long id) {
		List<PermissionDtoInterface> permission = this.permissionService.getPermissionById(id);
		return ResponseEntity.ok(permission);
	}

	@PutMapping("/{id}")
	public ResponseEntity<?> updatePermission(@RequestBody PermissionDto permissionDto, @PathVariable Long id) {
		PermissionDto updatedPermission = this.permissionService.updatePermission(permissionDto, id);
		return new ResponseEntity<APIResponse>(new APIResponse("Permission updated successfully", updatedPermission),
				HttpStatus.OK);
	}

	public ResponseEntity<?> deletePermission(@PathVariable Long id) {
		this.permissionService.deletePermission(id);

		return new ResponseEntity<APIResponse>(new APIResponse("Permission deleted successfully"), HttpStatus.OK);
	}
}
