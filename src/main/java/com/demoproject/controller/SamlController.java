package com.demoproject.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.demoproject.serviceimpl.SamlService;

@RestController
@RequestMapping("/saml")
public class SamlController {

	@Autowired
	private SamlService samlService;

	@GetMapping("/response")
	public String samlResponse() {
		try {
			System.err.println("saml");
			this.samlService.sendSamlRequest();
			return "Success";
		} catch (Exception e) {
			e.printStackTrace();
			return null;

		}
	}
}
