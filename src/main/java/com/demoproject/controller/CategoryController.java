package com.demoproject.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.demoproject.payload.APIResponse;
import com.demoproject.payload.CategoryDto;
import com.demoproject.payload.CategoryDtoInterface;
import com.demoproject.serviceinterfaces.CategoryServiceInterface;

@RestController
@RequestMapping("/category")
public class CategoryController {

	@Autowired
	private CategoryServiceInterface categoryService;

	@PreAuthorize("hasRole('CategoryAdd1')")
	@PostMapping
	public ResponseEntity<APIResponse> createCategory(@Valid @RequestBody CategoryDto categoryDto) {

		CategoryDto createdCategoryDto = this.categoryService.createCategory(categoryDto);

		return new ResponseEntity<APIResponse>(new APIResponse("Category Created successfully", createdCategoryDto),
				HttpStatus.CREATED);
	}

	@PreAuthorize("hasRole('CategoryUpdate')")
	@PutMapping("/{categoryId}")
	public ResponseEntity<APIResponse> updateCategory(@RequestBody CategoryDto categoryDto,
			@PathVariable("categoryId") Long categoryId) {

		CategoryDto updatedCategoryDto = this.categoryService.updateCategory(categoryDto, categoryId);

		return new ResponseEntity<APIResponse>(new APIResponse("Category Created successfully", updatedCategoryDto),
				HttpStatus.OK);
	}

	@PreAuthorize("hasRole('CategoryDelete')")
	@DeleteMapping("/{categoryId}")
	public ResponseEntity<APIResponse> deleteCategory(@PathVariable("categoryId") Long categoryId) {
		CategoryDto deletedCategoryDto = this.categoryService.deleteCategory(categoryId);

		return new ResponseEntity<APIResponse>(new APIResponse("Category Deleted Successfully", deletedCategoryDto),
				HttpStatus.OK);
	}

	@PreAuthorize("hasRole('CategoryView')")
	@GetMapping("/{categoryId}")
	public ResponseEntity<List<CategoryDtoInterface>> getCategoryById(@PathVariable("categoryId") Long categoryId) {
		List<CategoryDtoInterface> list = this.categoryService.getCategoryById(categoryId);

		return ResponseEntity.ok(list);
	}

	@PreAuthorize("hasRole('CategoryList')")
	@GetMapping
	public ResponseEntity<List<CategoryDtoInterface>> getAllCategories() {
		List<CategoryDtoInterface> list = this.categoryService.getAllCategories();

		return ResponseEntity.ok(list);
	}
}
