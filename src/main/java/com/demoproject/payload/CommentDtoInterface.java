package com.demoproject.payload;

public interface CommentDtoInterface {

	Long getId();
	
	String getContent();
}
