package com.demoproject.payload;

public class ErrorResponseDto {

	private String key;

	private String message;

	public ErrorResponseDto() {
		super();

	}

	public ErrorResponseDto(String message, String key) {
		super();
		this.message = message;
		this.key = key;
	}

	public ErrorResponseDto(String message) {
		super();
		this.message = message;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

}
