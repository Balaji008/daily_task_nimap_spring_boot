package com.demoproject.payload;

public interface UserRoleDtoInterface {

	Long getUserId();

	Long getRoleId();
}
