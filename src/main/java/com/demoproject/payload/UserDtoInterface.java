package com.demoproject.payload;

public interface UserDtoInterface {

	public Long getId();
	
	public String getName();
	
	public String getEmail();
	
	public String getAbout();
}
