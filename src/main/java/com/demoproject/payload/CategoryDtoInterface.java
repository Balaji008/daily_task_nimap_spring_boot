package com.demoproject.payload;

public interface CategoryDtoInterface {

	Long getCategoryId();

	String getCategoryTitle();

	String getCategoryDescription();
}
