package com.demoproject.payload;

public interface RolePermissionDtoInterface {

	Long getRoleId();

	Long getPermissionId();
}
