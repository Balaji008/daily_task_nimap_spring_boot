package com.demoproject.payload;

import java.util.List;

public interface PostDtoInterface {

	Long getPostId();

	String getTitle();

	String getContent();

	String getImageName();

	UserDtoInterface1 getUser();

	CategoryDtoInterface1 getCategory();

	List<CommentDtoInterface> getComments();

}
