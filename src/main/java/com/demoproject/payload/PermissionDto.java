package com.demoproject.payload;

public class PermissionDto {

	private String actionName;

	public PermissionDto() {
		super();

	}

	public PermissionDto(String actionName) {
		super();
		this.actionName = actionName;
	}

	public String getActionName() {
		return actionName;
	}

	public void setActionName(String actionName) {
		this.actionName = actionName;
	}

}
