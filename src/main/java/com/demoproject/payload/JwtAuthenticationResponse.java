package com.demoproject.payload;

import java.util.ArrayList;

public class JwtAuthenticationResponse {
	private String accessToken;

	private String refreshToken;

	private ArrayList<String> permissions;

	public ArrayList<String> getPermissions() {
		return permissions;
	}

	public void setPermissions(ArrayList<String> permissions) {
		this.permissions = permissions;
	}

	public JwtAuthenticationResponse(String accessToken, String refreshToken, ArrayList<String> permissions) {
		super();
		this.accessToken = accessToken;
		this.refreshToken = refreshToken;
		this.permissions = permissions;
	}

	public String getAccessToken() {
		return accessToken;
	}

	public void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
	}

	public String getRefreshToken() {
		return refreshToken;
	}

	public void setRefreshToken(String refreshToken) {
		this.refreshToken = refreshToken;
	}

	public JwtAuthenticationResponse(String accessToken, String refreshToken) {
		super();
		this.accessToken = accessToken;
		this.refreshToken = refreshToken;
	}

	public JwtAuthenticationResponse(String accessToken, ArrayList<String> permissions) {
		super();
		this.accessToken = accessToken;
		this.permissions = permissions;
	}

	public JwtAuthenticationResponse() {
		super();
		// TODO Auto-generated constructor stub
	}

}
