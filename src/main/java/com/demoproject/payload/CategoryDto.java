package com.demoproject.payload;

import javax.validation.constraints.NotEmpty;

public class CategoryDto {

	@NotEmpty(message = "Category Title is Required")
	private String categoryTitle;

	@NotEmpty(message = "Category Description is Required")
	private String categoryDescription;

	public CategoryDto(String categoryTitle, String categoryDescription) {
		super();
		this.categoryTitle = categoryTitle;
		this.categoryDescription = categoryDescription;
	}

	public CategoryDto() {
		super();
		// TODO Auto-generated constructor stub
	}

	public String getCategoryTitle() {
		return categoryTitle;
	}

	public void setCategoryTitle(String categoryTitle) {
		this.categoryTitle = categoryTitle;
	}

	public String getCategoryDescription() {
		return categoryDescription;
	}

	public void setCategoryDescription(String categoryDescription) {
		this.categoryDescription = categoryDescription;
	}
}
