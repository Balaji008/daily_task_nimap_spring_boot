package com.demoproject.payload;

public class APIResponse {

	private String message;

	private Object Data;

	public APIResponse() {
		super();
		// TODO Auto-generated constructor stub
	}

	public APIResponse(String message, Object data) {
		super();
		this.message = message;
		Data = data;
	}

	public APIResponse(String message) {
		this.message = message;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Object getData() {
		return Data;
	}

	public void setData(Object data) {
		Data = data;
	}

}
