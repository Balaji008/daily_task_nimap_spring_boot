package com.demoproject.payload;

import javax.validation.constraints.NotEmpty;

public class PostDto {

	@NotEmpty(message = "Post title is required")
	private String title;

	@NotEmpty(message = "Post content is required")
	private String content;

	private String imageName;

	private Long userId;

	private Long categoryId;

//	List<Comment> comments = new ArrayList<Comment>();
//
//	public List<Comment> getComments() {
//		return comments;
//	}
//
//	public void setComments(List<Comment> comments) {
//		this.comments = comments;
//	}

	public String getImageName() {
		return imageName;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public Long getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(Long categoryId) {
		this.categoryId = categoryId;
	}

	public void setImageName(String imageName) {
		this.imageName = imageName;
	}

	public PostDto() {
		super();
		// TODO Auto-generated constructor stub
	}

	public PostDto(@NotEmpty(message = "Post title is required") String title,
			@NotEmpty(message = "Post content is required") String content, String imageName, Long userId,
			Long categoryId) {
		super();
		this.title = title;
		this.content = content;
		this.imageName = imageName;
		this.userId = userId;
		this.categoryId = categoryId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

}
