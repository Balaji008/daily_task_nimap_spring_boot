package com.demoproject.payload;

import java.util.UUID;

public class JwtTokenRequest {
	private UUID uuid;

	private String password;

	public String getPassword() {
		return password;
	}

	public UUID getUuid() {
		return uuid;
	}

	public void setUuid(UUID uuid) {
		this.uuid = uuid;
	}

	public void setPassword(String password) {
		this.password = password;
	}
}
