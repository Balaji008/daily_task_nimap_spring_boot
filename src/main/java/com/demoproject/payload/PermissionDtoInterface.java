package com.demoproject.payload;

public interface PermissionDtoInterface {

	Long getId();

	String getActionName();
}
