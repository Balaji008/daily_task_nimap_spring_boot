package com.demoproject.payload;

public interface IListRolePermissionDto {

	Long getId();

	Long getRoleId();

	String getRoleName();

	Long getPermissionId();

	String getPermissionName();
}
