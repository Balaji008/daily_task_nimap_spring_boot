package com.demoproject.payload;

import java.util.List;

public class PostResponse {

	private List<PostDtoInterface> content;

	private Integer pageNumber;

	private Integer pageSize;

	private Integer totalPages;

	private Long totalElements;

	private boolean lastPage;

	public List<PostDtoInterface> getContent() {
		return content;
	}

	public void setContent(List<PostDtoInterface> content) {
		this.content = content;
	}

	public Integer getPageNumber() {
		return pageNumber;
	}

	public void setPageNumber(Integer pageNumber) {
		this.pageNumber = pageNumber;
	}

	public Integer getPageSize() {
		return pageSize;
	}

	public void setPageSize(Integer pageSize) {
		this.pageSize = pageSize;
	}

	public Integer getTotalPages() {
		return totalPages;
	}

	public void setTotalPages(Integer totalPages) {
		this.totalPages = totalPages;
	}

	public Long getTotalElements() {
		return totalElements;
	}

	public void setTotalElements(Long totalElements) {
		this.totalElements = totalElements;
	}

	public boolean isLastPage() {
		return lastPage;
	}

	public void setLastPage(boolean lastPage) {
		this.lastPage = lastPage;
	}

	public PostResponse(List<PostDtoInterface> content, Integer pageNumber, Integer pageSize, Integer totalPages,
			Long totalElements, boolean lastPage) {
		super();
		this.content = content;
		this.pageNumber = pageNumber;
		this.pageSize = pageSize;
		this.totalPages = totalPages;
		this.totalElements = totalElements;
		this.lastPage = lastPage;
	}

	public PostResponse() {
		super();
		// TODO Auto-generated constructor stub
	}

}
