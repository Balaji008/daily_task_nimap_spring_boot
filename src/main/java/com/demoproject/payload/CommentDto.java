package com.demoproject.payload;

public class CommentDto {

	private String content;

	public CommentDto() {
		super();
		// TODO Auto-generated constructor stub
	}

	public CommentDto(String content) {
		super();
		this.content = content;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}
}
