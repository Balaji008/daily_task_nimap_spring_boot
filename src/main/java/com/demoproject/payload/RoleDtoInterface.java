package com.demoproject.payload;

public interface RoleDtoInterface {
	Long getRoleId();

	String getName();
}
