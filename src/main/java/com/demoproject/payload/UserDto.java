package com.demoproject.payload;

import java.util.UUID;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

public class UserDto {

	@NotEmpty(message = "Name is Required")
	@Size(min = 4, message = "Username must be of 4 characters")
	private String name;

	@NotEmpty(message = "Email is Required")
	@Email(message = "Email address is not valid")
	private String email;

	@NotEmpty(message = "Password is Required")
	@Size(min = 8, max = 15, message = "Password must be of min 8 characters and maximum of 15 characters")
	private String password;

	//@NotNull(message = "About is Required")
	private String about;

	private UUID uuid;

	public UUID getUuid() {
		return uuid;
	}

	public void setUuid(UUID uuid) {
		this.uuid = uuid;
	}

	public UserDto() {
		super();
		// TODO Auto-generated constructor stub
	}

	public UserDto(String name, String email, String password, String about) {
		super();

		this.name = name;
		this.email = email;
		this.password = password;
		this.about = about;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getAbout() {
		return about;
	}

	public void setAbout(String about) {
		this.about = about;
	}

}
