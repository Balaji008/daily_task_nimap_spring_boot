package com.demoproject.serviceinterfaces;

import java.util.ArrayList;
import java.util.List;

import com.demoproject.payload.IListRolePermissionDto;
import com.demoproject.payload.RolePermissionDto;

public interface RolePermissionServiceInterface {

	public RolePermissionDto updateRolePermission(RolePermissionDto rolePermissionDto, Long id);

	public void deleteRolePermission(Long id);

	public List<IListRolePermissionDto> getAllRolePermissions();

	RolePermissionDto assignPermissionToRole(RolePermissionDto rolePermissionDto);

	public ArrayList<String> getAllPermissionsByUserId(Long userId);
}
