package com.demoproject.serviceinterfaces;

import java.util.List;

import com.demoproject.payload.UserRoleDto;
import com.demoproject.payload.UserRoleDtoInterface;

public interface UserRoleServiceInterface {

	UserRoleDto addRoleToUser(UserRoleDto userRoleDto);

	UserRoleDto updateUserRole(UserRoleDto userRoleDto, Long Id);

	List<UserRoleDtoInterface> getUserRoleByUserId(Long userId);

	List<UserRoleDtoInterface> getUserRoleByRoleId(Long RoleId);

	List<UserRoleDtoInterface> getAllUserRoles();

	void deleteUserRole(Long Id);

}
