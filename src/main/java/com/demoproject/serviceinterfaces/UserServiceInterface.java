package com.demoproject.serviceinterfaces;

import java.util.ArrayList;
import java.util.List;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import com.demoproject.payload.UserDto;
import com.demoproject.payload.UserDtoInterface;

public interface UserServiceInterface {

	UserDto createUser(UserDto user);

	UserDto updateUser(UserDto user, Long userId);

	List<UserDtoInterface> getUserById(Long userId);

	List<UserDtoInterface> getAllUsers();

	void deleteUser(Long userId);

	ArrayList<String> getAllPermissionsByUserId(Long userId);

	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException;

	public Boolean comparePassword(String password, String hashPassword);
}
