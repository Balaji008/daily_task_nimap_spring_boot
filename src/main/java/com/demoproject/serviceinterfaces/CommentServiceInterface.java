package com.demoproject.serviceinterfaces;

import com.demoproject.payload.CommentDto;

public interface CommentServiceInterface {

	CommentDto createComment(CommentDto commentDto,Long userId, Long postId);

	void deleteComment(Long commentId);
}
