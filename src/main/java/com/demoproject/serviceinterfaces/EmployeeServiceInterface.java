package com.demoproject.serviceinterfaces;

import java.util.List;

import com.demoproject.entities.Employee;

public interface EmployeeServiceInterface {

	public List<Employee> getAllEmployees();
	public Employee getEmployeeById(int id);
	public Employee addEmployee(Employee emp);
	public void deleteEmployeeById(int id);
	public Employee updateEmployeById(Employee emp, int id);
}
