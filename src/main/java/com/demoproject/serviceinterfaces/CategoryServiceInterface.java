package com.demoproject.serviceinterfaces;

import java.util.List;

import com.demoproject.payload.CategoryDto;
import com.demoproject.payload.CategoryDtoInterface;

public interface CategoryServiceInterface {

	CategoryDto createCategory(CategoryDto categoryDto);

	CategoryDto updateCategory(CategoryDto categoryDto, Long categoryId);

	CategoryDto deleteCategory(Long categoryId);

	List<CategoryDtoInterface> getCategoryById(Long categoryId);
	
	List<CategoryDtoInterface> getAllCategories();

}
