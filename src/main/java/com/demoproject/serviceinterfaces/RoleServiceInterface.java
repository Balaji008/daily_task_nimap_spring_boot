package com.demoproject.serviceinterfaces;

import java.util.List;

import com.demoproject.payload.RoleDto;
import com.demoproject.payload.RoleDtoInterface;

public interface RoleServiceInterface {
	public RoleDto addRole(RoleDto roleDto);

	public RoleDto deleteRole(Long roleId);

	public List<RoleDtoInterface> getAllRoles();

	List<RoleDtoInterface> getRoleByRoleId(Long roleId);

	RoleDto updateRole(RoleDto roleDto, Long roleId);
}
