package com.demoproject.serviceinterfaces;

import java.util.List;

import com.demoproject.payload.PostDto;
import com.demoproject.payload.PostDtoInterface;
import com.demoproject.payload.PostResponse;

public interface PostServiceInterfaces {

	// Create
	//PostDto createPost(PostDto postDto, Long userId, Long categoryId);
	PostDto createPost(PostDto postDto);

	// Update
	//PostDto updatePost(PostDto postDto, Long postId, Long userId, Long categoryId);
	PostDto updatePost(PostDto postDto, Long postId);

	// Delete
	PostDto deletePost(Long postId);

	// All posts
	PostResponse getAllPosts(Integer pageNumber,Integer pageSize,String sortBy);

	// Single post by post Id
	List<PostDtoInterface> getPostById(Long postId);

	// Get all posts by category
	List<PostDtoInterface> getPostsByCategory(Long categoryId);

	// Get all posts by User
	List<PostDtoInterface> getPostsByUser(Long userId);

	// Search posts by Keyword
	List<PostDtoInterface> searchPosts(String keyword);

	



}
