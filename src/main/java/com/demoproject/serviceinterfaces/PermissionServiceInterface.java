package com.demoproject.serviceinterfaces;

import java.util.List;

import com.demoproject.payload.PermissionDto;
import com.demoproject.payload.PermissionDtoInterface;

public interface PermissionServiceInterface {

	List<PermissionDtoInterface> getAllPermissions();

	PermissionDto createPermission(PermissionDto permissionDto);

	List<PermissionDtoInterface> getPermissionById(Long id);

	PermissionDto updatePermission(PermissionDto permissionDto, Long id);

	void deletePermission(Long id);
}
