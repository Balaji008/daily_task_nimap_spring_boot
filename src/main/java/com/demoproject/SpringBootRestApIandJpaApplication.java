package com.demoproject;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;



@SpringBootApplication
//(exclude =  {SecurityAutoConfiguration.class })
@EnableJpaRepositories
//@ComponentScan
//@EntityScan
//@EnableConfigurationProperties
public class SpringBootRestApIandJpaApplication //implements CommandLineRunner 
{

//	@Autowired
//	private PasswordEncoder passwordEncoder;

	public static void main(String[] args) {
		SpringApplication.run(SpringBootRestApIandJpaApplication.class, args);
	}

//	@Override
//	public void run(String... args) throws Exception {
//		System.out.println(this.passwordEncoder.encode("Ravmesh@1234"));
//		System.out.println(this.passwordEncoder.encode("Balaji@1234"));
//	}

}
